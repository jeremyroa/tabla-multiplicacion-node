const fs = require('fs')
const colors = require('colors')




let data = ''

const listarTabla = async (base, limite = 10) => {
  if (!Number(base) || !Number(limite)){
    throw Error(`No es un número ${ base } o ${limite}`)
  }
  for (let i = 1; i <= limite; ++i){
    data += `${base} * ${i} = ${base * i}\n`
  }
  return data
}
const crearArchivo = (base, limite = 10) => {
  return new Promise((resolve, reject) => {

    if (!Number(base) || !Number(limite)) {
      reject(`No es un numero ${ base } o ${limite}`)
      return
    }

    for (let i = 1; i <= limite; ++i)
      data += `${base} * ${i} = ${base * i}\n`

    fs.writeFile(`./archivos/base-${base}.txt`, data, (err) => {

      if (err) {
        reject(err)
        return
      }

      resolve(`base-${base}.txt`)

    });

  })

}

module.exports = {
  crearArchivo,
  listarTabla
}