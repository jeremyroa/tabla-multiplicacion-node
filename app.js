/**
 * @name Jeremy ROa
 * @argument Ejm de FileSystem Node JS
 * @date 05/12/2018
 */

const argv = require('./config/yargs').argv
const {
  crearArchivo,
  listarTabla
} = require('./multiplicar/multiplicar')
const colors = require('colors')



let comando = argv._[0]

switch (comando) {
  case 'listar':
    listarTabla(argv.base, argv.limite)
      .then(c => console.log(c))
      .catch(err => console.log(err))
    break;
  case 'crear':
    crearArchivo(argv.base, argv.limite)
      .then(arch => console.log('Se guardo en'.cyan,colors.green(arch)))
      .catch(err => console.log(err))
    break;

  default:
    console.log('No es un comando principal de app')
    break;
}