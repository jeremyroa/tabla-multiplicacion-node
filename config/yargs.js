

const opts = {
  base: {
    demand: true,
    alias: 'b'
  },
  limite: {
    alias: 'l',
    default: 10
  }
}

const argv = require('yargs')
  .command('listar', 'Muestra por pantalla la tabla de multiplicacion', opts)
  .command('crear', 'Guarda en un archivo la tabla de multiplicar', opts)
  .help()
  .argv

module.exports = {
  argv
}